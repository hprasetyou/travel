<?php
/**
 *
 */
class Auth extends CI_Controller
{

  function __construct()
  {
    # code...
    parent::__construct();
  }

  function index(){
    $this->template->render('auth/login');
  }

  function do_login(){
    $login = false;
    if($this->input->post('Username') && $this->input->post('Password')){
      $user = UserQuery::create()->findOneByName($this->input->post('Username'));
      if($user){
        if(password_verify( $this->input->post('Password'), $user->getPassword())){
          $login = true;
        }
      }
    }
    if($login){
      /*
      after user is valid, get user group
      save user session
      save user group in session
      save user access(from group) on session
      */
      $user_access = [];
      $redirect_url = [1=>'manage_users',2=>'manage_reservations',3=>'manage_reservations'];

      $newdata = array(
        'uid'  => $user->getId(),
        'logged_in' => True
      );
      $this->session->set_userdata($newdata);

    }else{
      $this->session->set_flashdata('login', 'Login failed');
      redirect('/login');
    }
    redirect($redirect_url[$user->getLevel()]);
  }

  function register(){
    if($this->input->post()){
      $user = new User();
      $user->setName($this->input->post('email'));
      $user->setPassword($this->input->post('password'));
      $user->setLevel(3);
      $user->save();
      $customer = new Customer();
      $customer->setName($this->input->post('name'));
      $customer->setIdCard($this->input->post('id_no'));
      $customer->setIdCardType('ktp');
      $customer->setGender($this->input->post('gender'));
      $customer->setAddress($this->input->post('address'));
      $customer->setPhone($this->input->post('phone'));
      $customer->setEmail($this->input->post('email'));
      $customer->setUser($user);
      $customer->save();
      redirect('/login');
      $this->session->set_flashdata('register', 'Registrasi berhasil, silahkan login menggunakan email dan password anda');
    }else{
      $this->template->render('auth/register');
    }
  }

  function do_logout(){
    $this->session->sess_destroy();
    redirect('auth');
  }

}
