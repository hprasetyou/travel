<?php

class Manage_customers extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->objname = 'Customer';
		$this->tpl = 'customers';

    // $this->authorization->check_authorization('manage_customers');
  }


  function create(){
		$this->template->render('admin/customers/form');
  }

	function write($id=null){
		$this->form = array(
     'IdCard' => 'IdCard',
     'IdCardType' => 'IdCardType',
     'Name' => 'Name',
     'Gender' => 'Gender',
     'Address' => 'Address',
     'Phone' => 'Phone',
     'Email' => 'Email',
    );
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('manage_customers/detail/'.$data->getId());
		}
	}


  function delete($id){
		$data = parent::delete($id);
		redirect('manage_customers');
  }

}
