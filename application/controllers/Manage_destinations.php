<?php


class Manage_destinations extends CI_Controller{


  function __construct(){
    parent::__construct();
   // $this->authorization->check_authorization('manage_destinations');
  }
  function index(){
      $this->template->render('admin/destinations/index');
  }

	function get_json(){
		$destinations = DestinationQuery::create();
		$maxPerPage = $this->input->get('length');
		if($this->input->get('search[value]')){

    }
		$offset = ($this->input->get('start')?$this->input->get('start'):0);
		$destinations = $destinations->paginate(($offset/10)+1, $maxPerPage);
    $o = [];
    $o['recordsTotal']=$destinations->getNbResults();
    $o['recordsFiltered']=$destinations->getNbResults();
    $o['draw']=$this->input->get('draw');
    $o['data']=[];
    $i=0;
    foreach ($destinations as $destination) {
				$o['data'][$i]['id'] = $destination->getId();
				$o['data'][$i]['name'] = $destination->getName();
				$o['data'][$i]['city'] = $destination->getCity();

				$i++;
    }
		echo json_encode($o);
	}

  function create(){
		
		$this->template->render('admin/destinations/form',array());
  }

  function detail($id){
		
		$destination = DestinationQuery::create()->findPK($id);
		$this->template->render('admin/destinations/form',array('destinations'=>$destination,));
  }

  function write($id=null){
		if($id){
			$destination = DestinationQuery::create()->findPK($id);
		}else{
			$destination = new Destination;
		}
		$destination->setName($this->input->post('Name'));
		$destination->setCity($this->input->post('City'));

		$destination->save();
		//$this->loging->add_entry('destinations',$destination->getId(),($id?'melakukan perubahan pada data':'membuat data baru'));
		redirect('manage_destinations/detail/'.$destination->getId());
  }

  function delete($id){
		if($this->input->post('confirm') == 'Ya'){
			$destination = DestinationQuery::create()->findPK($id);
			$destination->delete();
		}
		redirect('manage_destinations');
  }

}
    