<?php

class Manage_payments extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Payment');
		$this->tpl = 'payments';

  }

  function write($id=null){
    $this->form['ReservationId'] = 'ReservationId';
		$vehicle = parent::write($id);
		//$this->loging->add_entry('vehicles',$vehicle->getId(),($id?'melakukan perubahan pada data':'membuat data baru'));
		redirect('manage_payments/detail/'.$vehicle->getId());
  }

  function get_json(){
    $user = UserQuery::create()->findPk($this->session->userdata('uid'));
    $this->custom_code = "\$o['data'][\$i]['payment_status']=\$o['data'][\$i]['confirm']?'Dikonfirmasi':'Pending';";
    if($user->getLevel() == 3){
      $this->objobj = PaymentQuery::create()
        ->useReservationQuery()
            ->filterByCustomer(CustomerQuery::create()->findOneByUserId($this->session->userdata('uid')))
        ->endUse();
    }
    parent::get_json();
  }
  function pay_reservation($reservation_id){
    $payment = PaymentQuery::create()->findOneByReservationId($reservation_id);
    if(!$payment){
      $rent = ReservationQuery::create()->findPk($reservation_id);
      $totday = $rent->getEndTime()->diff($rent->getStartTime())->days + 1;
      $totpay = $totday * $rent->getRoute()->getBasePrice();
      $payment = new Payment();
      $payment->setReservationId($reservation_id);
      $payment->setRentFee($totpay);
      $payment->setTotal($totpay);
      $payment->save();
    }
    redirect('/manage_payments/detail/'.$payment->getId());

  }

  function confirm($id){
    $payment = PaymentQuery::create()->findPk($id);
    $payment->setConfirm(true);
    $payment->save();
    redirect('/manage_payments/detail/'.$payment->getId());
  }

  function upload_attachment($id){
    $config['upload_path']          = './public/uploads/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('attachment'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $payment = PaymentQuery::create()->findPk($id);
          $image = $this->upload->data();
          $payment->setAttachment('/public/uploads/'.$image['file_name']);
          $payment->save();
          redirect('/manage_payments/detail/'.$payment->getId());
      }
  }

  function print_denda(){
    $this->objobj = PaymentQuery::create()->filterByPenaltyFee(array('min' => 1));
    $this->print_report('print_report__denda');

  }
  function print_kwitansi($id){
    $data = PaymentQuery::create()->findPk($id);
    $this->template->render("admin/payments/pdf/report", array('payments'=>$data) );

  }
  function create(){
		$this->template->render('admin/payments/form');
  }

  function delete($id){
		$data = parent::delete($id);
		redirect('manage_payments');
  }

}
