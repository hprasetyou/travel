<?php

class Manage_reservations extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Reservation');
		$this->tpl = 'reservations';

    // $this->authorization->check_authorization('manage_reservations');
  }



    function create(){
  		$this->template->render('admin/reservations/form');
    }
      function generate_report(){
        if($this->input->post()){
          $res = ReservationQuery::create()
          ->filterByCreatedAt(array('min'=>$this->input->post('mulai'),'max'=>$this->input->post('selesai')))
          ->find();
      		$this->template->render_pdf('admin/reservations/report',array('reservations'=>$res));
        }
    		$this->template->render('admin/reservations/generate_report');
      }


	function write($id=null){
    $cust = CustomerQuery::create()->findOneByUserId($this->session->userdata('uid'));
		$this->form = array(
 'id' => 'id',
 'Code' => 'Code',
 'RouteId' => 'RouteId',
 'CustomerId' => array('value'=>$cust->getId()),
 'StartTime' => 'StartTime',
 'EndTime' => 'EndTime',
);
		$data = parent::write($id);
    if($this->input->is_ajax_request()){
			echo $data->toJSON();
		}else{
			redirect('manage_reservations/detail/'.$data->getId());
		}
	}
 function get_json(){
   $user = UserQuery::create()->findPk($this->session->userdata('uid'));
   $this->custom_code = "\$o['data'][\$i]['status_pengembalian']=\$o['data'][\$i]['return_time']?'Dikembalikan':'Belum dikembalikan';";
   if($user->getLevel() == 3){
     $this->objobj = ReservationQuery::create()
     ->filterByCustomer(CustomerQuery::create()->findOneByUserId($this->session->userdata('uid')));
   }
   parent::get_json();
 }
    function delete($id){
  		$data = parent::delete($id);
  		redirect('manage_reservations');
    }
      function set_return($id){
    		$data = ReservationQuery::create()->findPk($id);
        $payment = PaymentQuery::create()->findOneByReservationId($id);
        $rt = DateTime::createFromFormat('Y/m/d',$this->input->post('ReturnTime'));
        $telat = $rt->diff($data->getEndTime());
        $payment->setPenaltyFee($telat*100000);
        $payment->save();
        $data->setReturnTime($this->input->post('ReturnTime'));
        $data->save();
        if($telat>0){
    		redirect("manage_payments/detail/{$payment->id}");
        }
    		redirect("manage_reservations/detail/$id");
      }

}
