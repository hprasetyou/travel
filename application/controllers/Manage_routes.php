<?php


class Manage_routes extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Route');
		$this->tpl = 'routes';
   // $this->authorization->check_authorization('manage_routes');
  }
  function index(){
      $this->template->render('admin/routes/index');
  }

  function get_json(){
       $this->custom_code = "\$o['data'][\$i]['vehicle_img']=\$obj->getVehicle()->getImage();";
       parent::get_json();
  }


  function create(){

		$destinations = DestinationQuery::create()->find();

		$destinations = DestinationQuery::create()->find();

		$vehicles = VehicleQuery::create()->find();

		$this->template->render('admin/routes/form',array(
		'destinations'=> $destinations,

		'vehicles'=> $vehicles,
			));
  }


  function write($id=null){
      $this->form['DestId'] = 'DestId';
      $this->form['VehicleId'] = 'VehicleId';
      $route = parent::write($id);
      redirect('manage_routes/detail/'.$route->getId());
  }

  function delete($id){
		if($this->input->post('confirm') == 'Ya'){
			$route = RouteQuery::create()->findPK($id);
			$route->delete();
		}
		redirect('manage_routes');
  }

}
