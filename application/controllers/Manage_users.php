<?php


class Manage_users extends CI_Controller{


  function __construct(){
    parent::__construct();
   // $this->authorization->check_authorization('manage_users');
  }
  function index(){
      $this->template->render('admin/users/index');
  }

	function get_json(){
		$users = UserQuery::create();
		$maxPerPage = $this->input->get('length');
		if($this->input->get('search[value]')){

    }
		$offset = ($this->input->get('start')?$this->input->get('start'):0);
		$users = $users->paginate(($offset/10)+1, $maxPerPage);
    $o = [];
    $o['recordsTotal']=$users->getNbResults();
    $o['recordsFiltered']=$users->getNbResults();
    $o['draw']=$this->input->get('draw');
    $o['data']=[];
    $i=0;
    foreach ($users as $user) {
				$o['data'][$i]['id'] = $user->getId();
				$o['data'][$i]['name'] = $user->getName();
				$o['data'][$i]['password'] = $user->getPassword();
				$o['data'][$i]['level'] = (($user->getLevel()==1)?'Admin':'Operator');

				$i++;
    }
		echo json_encode($o);
	}

  function create(){

		$this->template->render('admin/users/form',array());
  }

  function detail($id){

		$user = UserQuery::create()->findPK($id);
		$this->template->render('admin/users/form',array('users'=>$user,));
  }

  function write($id=null){
		if($id){
			$user = UserQuery::create()->findPK($id);
		}else{
			$user = new User;
		}
		$user->setName($this->input->post('name'));
		$user->setPassword(password_hash($this->input->post('password'),PASSWORD_DEFAULT));
		$user->setLevel($this->input->post('level'));

		$user->save();
		//$this->loging->add_entry('users',$user->getId(),($id?'melakukan perubahan pada data':'membuat data baru'));
		redirect('manage_users/detail/'.$user->getId());
  }

  function delete($id){
		if($this->input->post('confirm') == 'Ya'){
			$user = UserQuery::create()->findPK($id);
			$user->delete();
		}
		redirect('manage_users');
  }

}
