<?php


class Manage_vehicles extends MY_Controller{


  function __construct(){
    parent::__construct();
		$this->set_objname('Vehicle',['Image']);
		$this->tpl = 'vehicles';
   // $this->authorization->check_authorization('manage_vehicles');
  }
  function index(){
      $this->template->render('admin/vehicles/index');
  }

  function create(){

		$this->template->render('admin/vehicles/form',array());
  }


  function write($id=null){
    $config['upload_path']          = './public/uploads/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('VehicleImage')){
      print_r($this->upload->display_errors());
    }else{
      $image = $this->upload->data();
      $this->form['Image'] = array('value'=>"/public/uploads/{$image['file_name']}");
    }
		$vehicle = parent::write($id);
		//$this->loging->add_entry('vehicles',$vehicle->getId(),($id?'melakukan perubahan pada data':'membuat data baru'));
		redirect('manage_vehicles/detail/'.$vehicle->getId());
  }

  function delete($id){
		if($this->input->post('confirm') == 'Ya'){
			$vehicle = VehicleQuery::create()->findPK($id);
			$vehicle->delete();
		}
		redirect('manage_vehicles');
  }

}
