<?php 
 return array(
'address' => 'Address',
'code' => 'Code',
'customer_id' => 'Customer Id',
'email' => 'Email',
'end_time' => 'End Time',
'gender' => 'Gender',
'id_card' => 'Id Card',
'id_card_type' => 'Id Card Type',
'message' => 'Message',
'name' => 'Name',
'penalty_fee' => 'Penalty Fee',
'phone' => 'Phone',
'rent_fee' => 'Rent Fee',
'reservation_id' => 'Reservation Id',
'route_id' => 'Route Id',
'start_time' => 'Start Time',
'total' => 'Total',
'verified_by' => 'Verified By'
);