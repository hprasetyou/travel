<?php

namespace Base;

use \Route as ChildRoute;
use \RouteQuery as ChildRouteQuery;
use \Exception;
use \PDO;
use Map\RouteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'route' table.
 *
 *
 *
 * @method     ChildRouteQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildRouteQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRouteQuery orderByDestId($order = Criteria::ASC) Order by the dest_id column
 * @method     ChildRouteQuery orderByVehicleId($order = Criteria::ASC) Order by the vehicle_id column
 * @method     ChildRouteQuery orderByBasePrice($order = Criteria::ASC) Order by the base_price column
 * @method     ChildRouteQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildRouteQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildRouteQuery groupByid() Group by the id column
 * @method     ChildRouteQuery groupByName() Group by the name column
 * @method     ChildRouteQuery groupByDestId() Group by the dest_id column
 * @method     ChildRouteQuery groupByVehicleId() Group by the vehicle_id column
 * @method     ChildRouteQuery groupByBasePrice() Group by the base_price column
 * @method     ChildRouteQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildRouteQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildRouteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRouteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRouteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRouteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRouteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRouteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRouteQuery leftJoinDestination($relationAlias = null) Adds a LEFT JOIN clause to the query using the Destination relation
 * @method     ChildRouteQuery rightJoinDestination($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Destination relation
 * @method     ChildRouteQuery innerJoinDestination($relationAlias = null) Adds a INNER JOIN clause to the query using the Destination relation
 *
 * @method     ChildRouteQuery joinWithDestination($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Destination relation
 *
 * @method     ChildRouteQuery leftJoinWithDestination() Adds a LEFT JOIN clause and with to the query using the Destination relation
 * @method     ChildRouteQuery rightJoinWithDestination() Adds a RIGHT JOIN clause and with to the query using the Destination relation
 * @method     ChildRouteQuery innerJoinWithDestination() Adds a INNER JOIN clause and with to the query using the Destination relation
 *
 * @method     ChildRouteQuery leftJoinVehicle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vehicle relation
 * @method     ChildRouteQuery rightJoinVehicle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vehicle relation
 * @method     ChildRouteQuery innerJoinVehicle($relationAlias = null) Adds a INNER JOIN clause to the query using the Vehicle relation
 *
 * @method     ChildRouteQuery joinWithVehicle($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vehicle relation
 *
 * @method     ChildRouteQuery leftJoinWithVehicle() Adds a LEFT JOIN clause and with to the query using the Vehicle relation
 * @method     ChildRouteQuery rightJoinWithVehicle() Adds a RIGHT JOIN clause and with to the query using the Vehicle relation
 * @method     ChildRouteQuery innerJoinWithVehicle() Adds a INNER JOIN clause and with to the query using the Vehicle relation
 *
 * @method     ChildRouteQuery leftJoinReservation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Reservation relation
 * @method     ChildRouteQuery rightJoinReservation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Reservation relation
 * @method     ChildRouteQuery innerJoinReservation($relationAlias = null) Adds a INNER JOIN clause to the query using the Reservation relation
 *
 * @method     ChildRouteQuery joinWithReservation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Reservation relation
 *
 * @method     ChildRouteQuery leftJoinWithReservation() Adds a LEFT JOIN clause and with to the query using the Reservation relation
 * @method     ChildRouteQuery rightJoinWithReservation() Adds a RIGHT JOIN clause and with to the query using the Reservation relation
 * @method     ChildRouteQuery innerJoinWithReservation() Adds a INNER JOIN clause and with to the query using the Reservation relation
 *
 * @method     \DestinationQuery|\VehicleQuery|\ReservationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRoute findOne(ConnectionInterface $con = null) Return the first ChildRoute matching the query
 * @method     ChildRoute findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRoute matching the query, or a new ChildRoute object populated from the query conditions when no match is found
 *
 * @method     ChildRoute findOneByid(int $id) Return the first ChildRoute filtered by the id column
 * @method     ChildRoute findOneByName(string $name) Return the first ChildRoute filtered by the name column
 * @method     ChildRoute findOneByDestId(int $dest_id) Return the first ChildRoute filtered by the dest_id column
 * @method     ChildRoute findOneByVehicleId(int $vehicle_id) Return the first ChildRoute filtered by the vehicle_id column
 * @method     ChildRoute findOneByBasePrice(int $base_price) Return the first ChildRoute filtered by the base_price column
 * @method     ChildRoute findOneByCreatedAt(string $created_at) Return the first ChildRoute filtered by the created_at column
 * @method     ChildRoute findOneByUpdatedAt(string $updated_at) Return the first ChildRoute filtered by the updated_at column *

 * @method     ChildRoute requirePk($key, ConnectionInterface $con = null) Return the ChildRoute by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOne(ConnectionInterface $con = null) Return the first ChildRoute matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRoute requireOneByid(int $id) Return the first ChildRoute filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByName(string $name) Return the first ChildRoute filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByDestId(int $dest_id) Return the first ChildRoute filtered by the dest_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByVehicleId(int $vehicle_id) Return the first ChildRoute filtered by the vehicle_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByBasePrice(int $base_price) Return the first ChildRoute filtered by the base_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByCreatedAt(string $created_at) Return the first ChildRoute filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoute requireOneByUpdatedAt(string $updated_at) Return the first ChildRoute filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRoute[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRoute objects based on current ModelCriteria
 * @method     ChildRoute[]|ObjectCollection findByid(int $id) Return ChildRoute objects filtered by the id column
 * @method     ChildRoute[]|ObjectCollection findByName(string $name) Return ChildRoute objects filtered by the name column
 * @method     ChildRoute[]|ObjectCollection findByDestId(int $dest_id) Return ChildRoute objects filtered by the dest_id column
 * @method     ChildRoute[]|ObjectCollection findByVehicleId(int $vehicle_id) Return ChildRoute objects filtered by the vehicle_id column
 * @method     ChildRoute[]|ObjectCollection findByBasePrice(int $base_price) Return ChildRoute objects filtered by the base_price column
 * @method     ChildRoute[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildRoute objects filtered by the created_at column
 * @method     ChildRoute[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildRoute objects filtered by the updated_at column
 * @method     ChildRoute[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RouteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RouteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Route', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRouteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRouteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRouteQuery) {
            return $criteria;
        }
        $query = new ChildRouteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRoute|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RouteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RouteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRoute A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, dest_id, vehicle_id, base_price, created_at, updated_at FROM route WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRoute $obj */
            $obj = new ChildRoute();
            $obj->hydrate($row);
            RouteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRoute|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RouteTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RouteTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the dest_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDestId(1234); // WHERE dest_id = 1234
     * $query->filterByDestId(array(12, 34)); // WHERE dest_id IN (12, 34)
     * $query->filterByDestId(array('min' => 12)); // WHERE dest_id > 12
     * </code>
     *
     * @see       filterByDestination()
     *
     * @param     mixed $destId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByDestId($destId = null, $comparison = null)
    {
        if (is_array($destId)) {
            $useMinMax = false;
            if (isset($destId['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_DEST_ID, $destId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($destId['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_DEST_ID, $destId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_DEST_ID, $destId, $comparison);
    }

    /**
     * Filter the query on the vehicle_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVehicleId(1234); // WHERE vehicle_id = 1234
     * $query->filterByVehicleId(array(12, 34)); // WHERE vehicle_id IN (12, 34)
     * $query->filterByVehicleId(array('min' => 12)); // WHERE vehicle_id > 12
     * </code>
     *
     * @see       filterByVehicle()
     *
     * @param     mixed $vehicleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByVehicleId($vehicleId = null, $comparison = null)
    {
        if (is_array($vehicleId)) {
            $useMinMax = false;
            if (isset($vehicleId['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_VEHICLE_ID, $vehicleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vehicleId['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_VEHICLE_ID, $vehicleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_VEHICLE_ID, $vehicleId, $comparison);
    }

    /**
     * Filter the query on the base_price column
     *
     * Example usage:
     * <code>
     * $query->filterByBasePrice(1234); // WHERE base_price = 1234
     * $query->filterByBasePrice(array(12, 34)); // WHERE base_price IN (12, 34)
     * $query->filterByBasePrice(array('min' => 12)); // WHERE base_price > 12
     * </code>
     *
     * @param     mixed $basePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByBasePrice($basePrice = null, $comparison = null)
    {
        if (is_array($basePrice)) {
            $useMinMax = false;
            if (isset($basePrice['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_BASE_PRICE, $basePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($basePrice['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_BASE_PRICE, $basePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_BASE_PRICE, $basePrice, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RouteTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RouteTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RouteTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Destination object
     *
     * @param \Destination|ObjectCollection $destination The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRouteQuery The current query, for fluid interface
     */
    public function filterByDestination($destination, $comparison = null)
    {
        if ($destination instanceof \Destination) {
            return $this
                ->addUsingAlias(RouteTableMap::COL_DEST_ID, $destination->getId(), $comparison);
        } elseif ($destination instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RouteTableMap::COL_DEST_ID, $destination->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDestination() only accepts arguments of type \Destination or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Destination relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function joinDestination($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Destination');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Destination');
        }

        return $this;
    }

    /**
     * Use the Destination relation Destination object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \DestinationQuery A secondary query class using the current class as primary query
     */
    public function useDestinationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDestination($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Destination', '\DestinationQuery');
    }

    /**
     * Filter the query by a related \Vehicle object
     *
     * @param \Vehicle|ObjectCollection $vehicle The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRouteQuery The current query, for fluid interface
     */
    public function filterByVehicle($vehicle, $comparison = null)
    {
        if ($vehicle instanceof \Vehicle) {
            return $this
                ->addUsingAlias(RouteTableMap::COL_VEHICLE_ID, $vehicle->getid(), $comparison);
        } elseif ($vehicle instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RouteTableMap::COL_VEHICLE_ID, $vehicle->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByVehicle() only accepts arguments of type \Vehicle or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vehicle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function joinVehicle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vehicle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vehicle');
        }

        return $this;
    }

    /**
     * Use the Vehicle relation Vehicle object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VehicleQuery A secondary query class using the current class as primary query
     */
    public function useVehicleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVehicle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vehicle', '\VehicleQuery');
    }

    /**
     * Filter the query by a related \Reservation object
     *
     * @param \Reservation|ObjectCollection $reservation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRouteQuery The current query, for fluid interface
     */
    public function filterByReservation($reservation, $comparison = null)
    {
        if ($reservation instanceof \Reservation) {
            return $this
                ->addUsingAlias(RouteTableMap::COL_ID, $reservation->getRouteId(), $comparison);
        } elseif ($reservation instanceof ObjectCollection) {
            return $this
                ->useReservationQuery()
                ->filterByPrimaryKeys($reservation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByReservation() only accepts arguments of type \Reservation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Reservation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function joinReservation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Reservation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Reservation');
        }

        return $this;
    }

    /**
     * Use the Reservation relation Reservation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ReservationQuery A secondary query class using the current class as primary query
     */
    public function useReservationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinReservation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Reservation', '\ReservationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRoute $route Object to remove from the list of results
     *
     * @return $this|ChildRouteQuery The current query, for fluid interface
     */
    public function prune($route = null)
    {
        if ($route) {
            $this->addUsingAlias(RouteTableMap::COL_ID, $route->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the route table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RouteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RouteTableMap::clearInstancePool();
            RouteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RouteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RouteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RouteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RouteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RouteQuery
