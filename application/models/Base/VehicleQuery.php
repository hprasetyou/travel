<?php

namespace Base;

use \Vehicle as ChildVehicle;
use \VehicleQuery as ChildVehicleQuery;
use \Exception;
use \PDO;
use Map\VehicleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vehicle' table.
 *
 *
 *
 * @method     ChildVehicleQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildVehicleQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildVehicleQuery orderByPlateNumber($order = Criteria::ASC) Order by the plate_number column
 * @method     ChildVehicleQuery orderByNumberOfSeat($order = Criteria::ASC) Order by the number_of_seat column
 * @method     ChildVehicleQuery orderByImage($order = Criteria::ASC) Order by the image column
 * @method     ChildVehicleQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildVehicleQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildVehicleQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildVehicleQuery groupByid() Group by the id column
 * @method     ChildVehicleQuery groupByName() Group by the name column
 * @method     ChildVehicleQuery groupByPlateNumber() Group by the plate_number column
 * @method     ChildVehicleQuery groupByNumberOfSeat() Group by the number_of_seat column
 * @method     ChildVehicleQuery groupByImage() Group by the image column
 * @method     ChildVehicleQuery groupByType() Group by the type column
 * @method     ChildVehicleQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildVehicleQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildVehicleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVehicleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVehicleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVehicleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVehicleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVehicleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVehicleQuery leftJoinRoute($relationAlias = null) Adds a LEFT JOIN clause to the query using the Route relation
 * @method     ChildVehicleQuery rightJoinRoute($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Route relation
 * @method     ChildVehicleQuery innerJoinRoute($relationAlias = null) Adds a INNER JOIN clause to the query using the Route relation
 *
 * @method     ChildVehicleQuery joinWithRoute($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Route relation
 *
 * @method     ChildVehicleQuery leftJoinWithRoute() Adds a LEFT JOIN clause and with to the query using the Route relation
 * @method     ChildVehicleQuery rightJoinWithRoute() Adds a RIGHT JOIN clause and with to the query using the Route relation
 * @method     ChildVehicleQuery innerJoinWithRoute() Adds a INNER JOIN clause and with to the query using the Route relation
 *
 * @method     \RouteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVehicle findOne(ConnectionInterface $con = null) Return the first ChildVehicle matching the query
 * @method     ChildVehicle findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVehicle matching the query, or a new ChildVehicle object populated from the query conditions when no match is found
 *
 * @method     ChildVehicle findOneByid(int $id) Return the first ChildVehicle filtered by the id column
 * @method     ChildVehicle findOneByName(string $name) Return the first ChildVehicle filtered by the name column
 * @method     ChildVehicle findOneByPlateNumber(string $plate_number) Return the first ChildVehicle filtered by the plate_number column
 * @method     ChildVehicle findOneByNumberOfSeat(string $number_of_seat) Return the first ChildVehicle filtered by the number_of_seat column
 * @method     ChildVehicle findOneByImage(string $image) Return the first ChildVehicle filtered by the image column
 * @method     ChildVehicle findOneByType(string $type) Return the first ChildVehicle filtered by the type column
 * @method     ChildVehicle findOneByCreatedAt(string $created_at) Return the first ChildVehicle filtered by the created_at column
 * @method     ChildVehicle findOneByUpdatedAt(string $updated_at) Return the first ChildVehicle filtered by the updated_at column *

 * @method     ChildVehicle requirePk($key, ConnectionInterface $con = null) Return the ChildVehicle by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOne(ConnectionInterface $con = null) Return the first ChildVehicle matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVehicle requireOneByid(int $id) Return the first ChildVehicle filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByName(string $name) Return the first ChildVehicle filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByPlateNumber(string $plate_number) Return the first ChildVehicle filtered by the plate_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByNumberOfSeat(string $number_of_seat) Return the first ChildVehicle filtered by the number_of_seat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByImage(string $image) Return the first ChildVehicle filtered by the image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByType(string $type) Return the first ChildVehicle filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByCreatedAt(string $created_at) Return the first ChildVehicle filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVehicle requireOneByUpdatedAt(string $updated_at) Return the first ChildVehicle filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVehicle[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVehicle objects based on current ModelCriteria
 * @method     ChildVehicle[]|ObjectCollection findByid(int $id) Return ChildVehicle objects filtered by the id column
 * @method     ChildVehicle[]|ObjectCollection findByName(string $name) Return ChildVehicle objects filtered by the name column
 * @method     ChildVehicle[]|ObjectCollection findByPlateNumber(string $plate_number) Return ChildVehicle objects filtered by the plate_number column
 * @method     ChildVehicle[]|ObjectCollection findByNumberOfSeat(string $number_of_seat) Return ChildVehicle objects filtered by the number_of_seat column
 * @method     ChildVehicle[]|ObjectCollection findByImage(string $image) Return ChildVehicle objects filtered by the image column
 * @method     ChildVehicle[]|ObjectCollection findByType(string $type) Return ChildVehicle objects filtered by the type column
 * @method     ChildVehicle[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildVehicle objects filtered by the created_at column
 * @method     ChildVehicle[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildVehicle objects filtered by the updated_at column
 * @method     ChildVehicle[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VehicleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VehicleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Vehicle', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVehicleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVehicleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVehicleQuery) {
            return $criteria;
        }
        $query = new ChildVehicleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVehicle|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VehicleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VehicleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVehicle A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, plate_number, number_of_seat, image, type, created_at, updated_at FROM vehicle WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVehicle $obj */
            $obj = new ChildVehicle();
            $obj->hydrate($row);
            VehicleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVehicle|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(VehicleTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(VehicleTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(VehicleTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(VehicleTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the plate_number column
     *
     * Example usage:
     * <code>
     * $query->filterByPlateNumber('fooValue');   // WHERE plate_number = 'fooValue'
     * $query->filterByPlateNumber('%fooValue%', Criteria::LIKE); // WHERE plate_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $plateNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByPlateNumber($plateNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($plateNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_PLATE_NUMBER, $plateNumber, $comparison);
    }

    /**
     * Filter the query on the number_of_seat column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfSeat('fooValue');   // WHERE number_of_seat = 'fooValue'
     * $query->filterByNumberOfSeat('%fooValue%', Criteria::LIKE); // WHERE number_of_seat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numberOfSeat The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByNumberOfSeat($numberOfSeat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numberOfSeat)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_NUMBER_OF_SEAT, $numberOfSeat, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%', Criteria::LIKE); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_IMAGE, $image, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(VehicleTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(VehicleTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(VehicleTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(VehicleTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VehicleTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Route object
     *
     * @param \Route|ObjectCollection $route the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildVehicleQuery The current query, for fluid interface
     */
    public function filterByRoute($route, $comparison = null)
    {
        if ($route instanceof \Route) {
            return $this
                ->addUsingAlias(VehicleTableMap::COL_ID, $route->getVehicleId(), $comparison);
        } elseif ($route instanceof ObjectCollection) {
            return $this
                ->useRouteQuery()
                ->filterByPrimaryKeys($route->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRoute() only accepts arguments of type \Route or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Route relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function joinRoute($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Route');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Route');
        }

        return $this;
    }

    /**
     * Use the Route relation Route object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RouteQuery A secondary query class using the current class as primary query
     */
    public function useRouteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRoute($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Route', '\RouteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVehicle $vehicle Object to remove from the list of results
     *
     * @return $this|ChildVehicleQuery The current query, for fluid interface
     */
    public function prune($vehicle = null)
    {
        if ($vehicle) {
            $this->addUsingAlias(VehicleTableMap::COL_ID, $vehicle->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vehicle table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VehicleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VehicleTableMap::clearInstancePool();
            VehicleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VehicleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VehicleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VehicleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VehicleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VehicleQuery
