<?php

use Base\Seat as BaseSeat;

/**
 * Skeleton subclass for representing a row from the 'seat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Seat extends BaseSeat
{

}
